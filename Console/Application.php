<?php

namespace Flyer\Console;

use Flyer\Registry;

class Application extends \Flyer\Console\Command {

	private static $apps = array();

	public function start($app) {
		Registry::save('_' . $app, $app);
	}

	public function commands($app) {
		if ($this->appExists($app)) {
			return parent::available();
		}
	}

	public function run($app, $command) {
		if ($this->appExists($app)) {
			if ($this->exists($command)) {
				// Run the command in our application
				return $this->runCommand($command);
			} else {
				echo 'Fout! Commando ' . $command . ' bestaat niet in ' . $app . '!';
			}
		}

	}

	public function create($command, $action, $arguments = array(), $description = null) {
		$this->add($command, $action, $arguments, $description);
	} 

	protected function appExists($app) {
		if (Registry::set('_' . $app)) {
		 	return true;
		} else {
			return false;
		}
	}
}