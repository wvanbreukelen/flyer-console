<?php

namespace Flyer\Console;

class CommandRegistry {

	/*
  	* All the saved commands ready to use
	*/

	protected $commands = array();

	/*
	* All the saved arguments for the commands
	*/

	protected $arguments = array();

	/*
	* All the saved descriptions for the commands
	*/

	protected $descriptions = array();

	/*
	* All the actions for the commands
	*/
	
	protected $actions = array();

	/*
	* Create a command and add it to the commandsRegistry
	*/

	public function create($command, $action, $arguments = array(), $description = null) {
		if ($this->exists($command)) {return;} 

		$this->commands[] = $command;
		$this->arguments[$command] = $arguments;
		$this->descriptions[$command] = $description;
		$this->actions[$command] = $action;
	}

	public function exists($command) {
		if (isset($this->commands[$command])) {
			return true;
		} else {
			return false;
		}
	}

	public function get($command) {

		$res = array();
		$res['name'] = $command;
		$res['action'] = $this->actions[$command];
		$res['arguments'] = $this->arguments[$command];
		$res['description'] = $this->descriptions[$command];

		return $res;
	}
}