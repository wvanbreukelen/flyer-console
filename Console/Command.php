<?php

namespace Flyer\Console;

use Flyer\Console\CommandRegistry;

class Command {

	private $_registry;

	public function __construct() {
		$this->_registry = new CommandRegistry();
	}

	public function runCommand($command) {
		$command = $this->_registry->get($command);
		$action = explode('@', $command['action']);

		require_once(APP . 'console' . DS . $action[0] . 'Console' . PHP_EXT);
		$obj = new $action[0];
		return $obj->$action[1]();
	}

	public function add($command, $action, $arguments = array(), $description = null) {
		$this->_registry->create($command, $action, $arguments, $description);
	}

	public function exists($command) {
		if (!$this->_registry->get($command)) {
			return false;
		}
		return true;
	}
}