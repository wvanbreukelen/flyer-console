<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', getcwd() . DS);
define('SYS', ROOT . 'Flyer' . DS);
define('APP', ROOT . 'App' . DS);
define('TEMPLATES', APP . 'templates' . DS);
define('PHP_EXT', '.php');


/*
|--------------------------------------------------------------------------
| Create The Artisan Application
|--------------------------------------------------------------------------
|
| Now we're ready to create the Artisan console application, which will
| be responsible for running the appropriate command.
|
*/

require(SYS . 'Registry' . PHP_EXT);
require(APP . 'config' . DS . 'packages' . PHP_EXT);
require(APP . 'config' . DS . 'database' . PHP_EXT);
require(APP . 'config' . DS . 'flyer' . PHP_EXT);

require(SYS . 'Console' . DS . 'CommandsRegistry' . PHP_EXT);
require(SYS . 'Console' . DS . 'Command'. PHP_EXT);
require(SYS . 'Console' . DS . 'Application'. PHP_EXT);


use Flyer\Console\Application;
use Flyer\Console\CommandRegistry;
$app = new Application();
$cmd = new CommandRegistry();

/*
|---------------------------------------------------------------------------
| Config the Artisan Application here!
|---------------------------------------------------------------------------
|
*/

$consoleCall = $_SERVER['argv'];
$command = $consoleCall[1];
$parameters = array_diff($consoleCall, array('artisan.php', $command, 'artisan'));

/*
|---------------------------------------------------------------------------
| Start/Prepare our Artisan Application
|---------------------------------------------------------------------------
|
| We are preparing our application so we can add commands to it.
|
*/

$app->start('FlyerApplication');
$app->create('test', 'general@test', 'name', 'A simple HelloWorld command!');

echo $app->run('FlyerApplication', $command);




